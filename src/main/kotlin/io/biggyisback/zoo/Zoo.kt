package io.biggyisback.zoo

object Zoo {
    private val animals = mutableListOf<Animal>()

    fun addAnimal(age: Int){
        animals.add(Animal(NameGenerator.getName(),age))
    }

    fun animalBirth(){
        animals.add(Animal(NameGenerator.getName()))
    }
}